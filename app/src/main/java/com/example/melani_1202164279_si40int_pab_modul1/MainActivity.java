package com.example.melani_1202164279_si40int_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText TextAlas;
    private EditText TextTinggi;
    private TextView TextHasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
        protected void hitung(View View){
            TextAlas = findViewById(R.id.editText2);
            TextTinggi = findViewById(R.id.editText3);
            TextHasil =findViewById(R.id.textView3);

            Integer Text_A = Integer.parseInt(TextAlas.getText().toString());
            Integer Text_T =Integer.parseInt(TextTinggi.getText().toString());
            Integer Text_H =Text_A*Text_T;
            TextHasil.setText(String.valueOf(Text_H));
        }
    }

